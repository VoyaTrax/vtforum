<?php
// $Id: main.php,v 1.2 2003/12/17 09:42:25 gij Exp $
//%%%%%%		Module Name phpBB  		%%%%%

define("_MD_VTFORUM_UPDATE","Update");
define("_MD_VTFORUM_UPDATED","Updated");

//functions.php
define("_MD_VTFORUM_ERROR","Error");
define("_MD_VTFORUM_NOPOSTS","No Posts");
define("_MD_VTFORUM_GO","Go");

//index.php
define("_MD_VTFORUM_FORUM","Forum");
define("_MD_VTFORUM_WELCOME","Welcome to %s Forum.");
define("_MD_VTFORUM_TOPICS","Topics");
define("_MD_VTFORUM_POSTS","Posts");
define("_MD_VTFORUM_LASTPOST","Last Post");
define("_MD_VTFORUM_MODERATOR","Moderator");
define("_MD_VTFORUM_NEWPOSTS","New posts");
define("_MD_VTFORUM_NONEWPOSTS","No new posts");
define("_MD_VTFORUM_PRIVATEFORUM","Private forum");
define("_MD_VTFORUM_BY","by"); // Posted by
define("_MD_VTFORUM_TOSTART","To start viewing messages, select the forum that you want to visit from the selection below.");
define("_MD_VTFORUM_TOTALTOPICSC","Total Topics: ");
define("_MD_VTFORUM_TOTALPOSTSC","Total Posts: ");
define("_MD_VTFORUM_TIMENOW","The time now is %s");
define("_MD_VTFORUM_LASTVISIT","You last visited: %s");
define("_MD_VTFORUM_ADVSEARCH","Advanced Search");
define("_MD_VTFORUM_POSTEDON","Posted on: ");
define("_MD_VTFORUM_SUBJECT","Subject");

//page_header.php
define("_MD_VTFORUM_MODERATEDBY","Moderated by");
define("_MD_VTFORUM_SEARCH","Search");
define("_MD_VTFORUM_SEARCHRESULTS","Search Results");
define("_MD_VTFORUM_FORUMINDEX","Forum Index");
define("_MD_VTFORUM_ALLTOPICSINDEX","Topic Index");
define("_MD_VTFORUM_POSTNEW","Post New Message");
define("_MD_VTFORUM_REGTOPOST","Register To Post");

//search.php
define("_MD_VTFORUM_KEYWORDS","Keywords:");
define("_MD_VTFORUM_SEARCHANY","Search for ANY of the terms");
define("_MD_VTFORUM_SEARCHALL","Search for ALL of the terms");
define("_MD_VTFORUM_SEARCHALLFORUMS","Search All Forums");
define("_MD_VTFORUM_FORUMC","Forum");
define("_MD_VTFORUM_SORTBY","Sort by");
define("_MD_VTFORUM_DATE","Date");
define("_MD_VTFORUM_TOPIC","Topic");
define("_MD_VTFORUM_USERNAME","Username");
define("_MD_VTFORUM_SEARCHIN","Search in");
define("_MD_VTFORUM_BODY","Body");
define("_MD_VTFORUM_NOMATCH","No records match that query. Please broaden your search.");
define("_MD_VTFORUM_POSTTIME","Post Time");

//viewforum.php
define("_MD_VTFORUM_LATESTPOST","latest");
define("_MD_VTFORUM_REPLIES","Replies");
define("_MD_VTFORUM_POSTER","Poster");
define("_MD_VTFORUM_VIEWS","Views");
define("_MD_VTFORUM_MORETHAN","New posts [ Popular ]");
define("_MD_VTFORUM_MORETHAN2","No New posts [ Popular ]");
define("_MD_VTFORUM_TOPICSTICKY","Topic is Sticky");
define("_MD_VTFORUM_TOPICLOCKED","Topic is Locked");
define("_MD_VTFORUM_LEGEND","Legend");
define("_MD_VTFORUM_NEXTPAGE","Next Page");
define("_MD_VTFORUM_SORTEDBY","Sorted by");
define("_MD_VTFORUM_FORUMNAME","forum name");
define("_MD_VTFORUM_TOPICTITLE","topic title");
define("_MD_VTFORUM_NUMBERREPLIES","number of replies");
define("_MD_VTFORUM_TOPICPOSTER","topic poster");
define("_MD_VTFORUM_LASTPOSTTIME","last post time");
define("_MD_VTFORUM_ASCENDING","Ascending order");
define("_MD_VTFORUM_DESCENDING","Descending order");
define("_MD_VTFORUM_FROMLASTDAYS","From last %s days");
define("_MD_VTFORUM_THELASTYEAR","From the last year");
define("_MD_VTFORUM_BEGINNING","From the beginning");
define("_MD_VTFORUM_WHRSOLVED","Status:");
define("_MD_VTFORUM_SOLVEDNO","UnSolved");
define("_MD_VTFORUM_SOLVEDYES","Solved");
define("_MD_VTFORUM_SOLVEDBOTH","Both");

//viewtopic.php
define("_MD_VTFORUM_AUTHOR","Author");
define("_MD_VTFORUM_LOCKTOPIC","Lock this topic");
define("_MD_VTFORUM_UNLOCKTOPIC","Unlock this topic");
define("_MD_VTFORUM_STICKYTOPIC","Make this topic Sticky");
define("_MD_VTFORUM_UNSTICKYTOPIC","Make this topic UnSticky");
define("_MD_VTFORUM_MOVETOPIC","Move this topic");
define("_MD_VTFORUM_DELETETOPIC","Delete this topic");
define("_MD_VTFORUM_TOP","Top");
define("_MD_VTFORUM_PARENT","Parent");
define("_MD_VTFORUM_PREVTOPIC","Previous Topic");
define("_MD_VTFORUM_NEXTTOPIC","Next Topic");

define("_MD_VTFORUM_MARK_TURNON","mark this topic");
define("_MD_VTFORUM_MARK_TURNOFF","unmark this topic");

//forumform.inc
define("_MD_VTFORUM_ABOUTPOST","About Posting");
define("_MD_VTFORUM_ANONCANPOST","<b>Anonymous</b> users can post new topics and replies to this forum");
define("_MD_VTFORUM_PRIVATE","This is a <b>Private</b> forum.<br />Only users with special access can post new topics and replies to this forum");define("_MD_VTFORUM_REGCANPOST","All <b>Registered</b> users can post new topics and replies to this forum");
define("_MD_VTFORUM_MODSCANPOST","Only <B>Moderators and Administrators</b> can post new topics and replies to this forum");
define("_MD_VTFORUM_PREVPAGE","Previous Page");
define("_MD_VTFORUM_QUOTE","Quote");

// ERROR messages
define("_MD_VTFORUM_ERRORFORUM","ERROR: Forum not selected!");
define("_MD_VTFORUM_ERRORPOST","ERROR: Post not selected!");
define("_MD_VTFORUM_NORIGHTTOPOST","You don't have the right to post in this forum.");
define("_MD_VTFORUM_NORIGHTTOACCESS","You don't have the right to access this forum.");
define("_MD_VTFORUM_ERRORTOPIC","ERROR: Topic not selected!");
define("_MD_VTFORUM_ERRORCONNECT","ERROR: Could not connect to the forums database.");
define("_MD_VTFORUM_ERROREXIST","ERROR: The forum you selected does not exist. Please go back and try again.");
define("_MD_VTFORUM_ERROROCCURED","An Error Occured");
define("_MD_VTFORUM_COULDNOTQUERY","Could not query the forums database.");
define("_MD_VTFORUM_FORUMNOEXIST","Error - The forum/topic you selected does not exist. Please go back and try again.");
define("_MD_VTFORUM_USERNOEXIST","That user does not exist.  Please go back and search again.");
define("_MD_VTFORUM_COULDNOTREMOVE","Error - Could not remove posts from the database!");
define("_MD_VTFORUM_COULDNOTREMOVETXT","Error - Could not remove post texts!");

//reply.php
define("_MD_VTFORUM_ON","on"); //Posted on
define("_MD_VTFORUM_USERWROTE","%s wrote:"); // %s is username

//post.php
define("_MD_VTFORUM_FORMTITLEINPREVIEW","Post from preview");
define("_MD_VTFORUM_EDITNOTALLOWED","You're not allowed to edit this post!");
define("_MD_VTFORUM_EDITEDBY","Edited by");
define("_MD_VTFORUM_ANONNOTALLOWED","Anonymous user not allowed to post.<br>Please register.");
define("_MD_VTFORUM_THANKSSUBMIT","Thanks for your submission!");
define("_MD_VTFORUM_THANKSEDIT","The post is modified");
define("_MD_VTFORUM_REPLYPOSTED","A reply to your topic has been posted.");
define("_MD_VTFORUM_HELLO","Hello %s,");
define("_MD_VTFORUM_URRECEIVING","You are receiving this email because a message you posted on %s forums has been replied to."); // %s is your site name
define("_MD_VTFORUM_CLICKBELOW","Click on the link below to view the thread:");
define("_MD_VTFORUM_FMT_GUESTSPOSTHEADER","Post from %s as a guest\n---\n\n");

//forumform.inc
define("_MD_VTFORUM_YOURNAME","Your Name:");
define("_MD_VTFORUM_LOGOUT","Logout");
define("_MD_VTFORUM_REGISTER","Register");
define("_MD_VTFORUM_SUBJECTC","Subject:");
define("_MD_VTFORUM_EDITMODEC","Mode:");
define("_MD_VTFORUM_ALERTEDIT","<div style='background:#FE0000;color:#FFFFFF;font-size:120%;font-weight:bold;padding:3px;'>You are now editing your topic</div>");
define("_MD_VTFORUM_GUESTNAMEC","Your Name:");
define("_MD_VTFORUM_UNAMEC","User:");
define("_MD_VTFORUM_FMT_UNAME","%s");
define("_MD_VTFORUM_MESSAGEICON","Message Icon:");
define("_MD_VTFORUM_SOLVEDCHECKBOX","Solved");
define("_MD_VTFORUM_MESSAGEC","Message:");
define("_MD_VTFORUM_ALLOWEDHTML","Allowed HTML:");
define("_MD_VTFORUM_OPTIONS","Options:");
define("_MD_VTFORUM_POSTANONLY","Post Anonymously");
define("_MD_VTFORUM_DISABLESMILEY","Disable Smiley");
define("_MD_VTFORUM_DISABLEHTML","Disable html");
define("_MD_VTFORUM_NEWPOSTNOTIFY", "Notify me of new posts in this thread");
define("_MD_VTFORUM_ATTACHSIG","Attach Signature");
define("_MD_VTFORUM_POST","Post");
define("_MD_VTFORUM_SUBMIT","Submit");
define("_MD_VTFORUM_CANCELPOST","Cancel Post");

// forumuserpost.php
define("_MD_VTFORUM_ADD","Add");
define("_MD_VTFORUM_REPLY","Reply");

// topicmanager.php
define("_MD_VTFORUM_YANTMOTFTYCPTF","You are not the moderator of this forum therefore you cannot perform this function.");
define("_MD_VTFORUM_TTHBRFTD","The topic has been removed from the database.");
define("_MD_VTFORUM_RETURNTOTHEFORUM","Return to the forum");
define("_MD_VTFORUM_RTTFI","Return to the forum index");
define("_MD_VTFORUM_EPGBATA","Error - Please go back and try again.");
define("_MD_VTFORUM_TTHBM","The topic has been moved.");
define("_MD_VTFORUM_VTUT","View the updated topic");
define("_MD_VTFORUM_TTHBL","The topic has been locked.");
define("_MD_VTFORUM_TTHBS","The topic has been Stickyed.");
define("_MD_VTFORUM_TTHBUS","The topic has been unStickyed.");
define("_MD_VTFORUM_VIEWTHETOPIC","View the topic");
define("_MD_VTFORUM_TTHBU","The topic has been unlocked.");
define("_MD_VTFORUM_OYPTDBATBOTFTTY","Once you press the delete button at the bottom of this form the topic you have selected, and all its related posts, will be <b>permanently</b> removed.");
define("_MD_VTFORUM_OYPTMBATBOTFTTY","Once you press the move button at the bottom of this form the topic you have selected, and its related posts, will be moved to the forum you have selected.");
define("_MD_VTFORUM_OYPTLBATBOTFTTY","Once you press the lock button at the bottom of this form the topic you have selected will be locked. You may unlock it at a later time if you like.");
define("_MD_VTFORUM_OYPTUBATBOTFTTY","Once you press the unlock button at the bottom of this form the topic you have selected will be unlocked. You may lock it again at a later time if you like.");
define("_MD_VTFORUM_OYPTSBATBOTFTTY","Once you press the Sticky button at the bottom of this form the topic you have selected will be Stickyed. You may unSticky it again at a later time if you like.");
define("_MD_VTFORUM_OYPTTBATBOTFTTY","Once you press the unSticky button at the bottom of this form the topic you have selected will be unStickyed. You may Sticky it again at a later time if you like.");
define("_MD_VTFORUM_MOVETOPICTO","Move Topic To:");
define("_MD_VTFORUM_NOFORUMINDB","No Forums in DB");
define("_MD_VTFORUM_DATABASEERROR","Database Error");
define("_MD_VTFORUM_DELTOPIC","Delete Topic");

// delete.php
define("_MD_VTFORUM_DELNOTALLOWED","Sorry, but you're not allowed to delete this post.");
define("_MD_VTFORUM_AREUSUREDEL","Are you sure you want to delete this post and all its child posts?");
define("_MD_VTFORUM_POSTSDELETED","Selected post and all its child posts deleted.");

// definitions moved from global.
define("_MD_VTFORUM_THREAD","Thread");
define("_MD_VTFORUM_FROM","From");
define("_MD_VTFORUM_JOINED","Joined");
define("_MD_VTFORUM_ONLINE","Online");
define("_MD_VTFORUM_BOTTOM","Bottom");
?>
