<?php
// $Id: blocks.php,v 1.1.1.1 2003/11/26 05:17:35 ryuji_amano Exp $
// Blocks
define("_MB_VTFORUM_FORUM","Forum");
define("_MB_VTFORUM_TOPIC","Topic");
define("_MB_VTFORUM_RPLS","Replies");
define("_MB_VTFORUM_VIEWS","Views");
define("_MB_VTFORUM_LPOST","Last Post");
define("_MB_VTFORUM_VSTFRMS","Forum Index");
define("_MB_VTFORUM_LISTALLTOPICS","Topic Index");
define("_MB_VTFORUM_DISPLAY","Display %s posts");
define("_MB_VTFORUM_DISPLAYF","Display in full size");
define("_MB_VTFORUM_MARKISUP","Marked topics are upper"); 
define("_MB_VTFORUM_DISPLAYPOSTTITLE","Displays the title of the last post insteadof the topic title");
define("_MB_VTFORUM_CATLIMIT","Specify categories");
define("_MB_VTFORUM_CATLIMITDSC","Input category's id separated with comma. blank means all categories");

define("_MB_VTFORUM_ORDERTIMED","Newest is first"); 
define("_MB_VTFORUM_ORDERVIEWSD","Most viewed is first"); 
define("_MB_VTFORUM_ORDERREPLIESD","Most commented is first"); 

define("_MB_VTFORUM_CLASSPUBLIC","Public forums only"); 
define("_MB_VTFORUM_CLASSBOTH","All type of forums"); 
define("_MB_VTFORUM_CLASSPRIVATE","Private forums only"); 

?>
